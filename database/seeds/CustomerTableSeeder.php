<?php

use Illuminate\Database\Seeder;
use Carbon\carbon;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
     {
        DB::table('customers')->insert([
            'name'       => 'John Doe',
            'address'    => 'Quezoon City',
            'created_at' => Carbon::now() ->format('y-m-d H:i:s'),
            'updated_at' => Carbon::now() ->format('y-m-d H:i:s'),
        ]);

        DB::table('customers')->insert([
            'name' => 'Jane Greg',
            'address' => 'Pasay City',
            'created_at' => Carbon::now() ->format('y-m-d H:i:s'),
            'updated_at' => Carbon::now() ->format('y-m-d H:i:s'),
        ]);
        DB::table('customers')->insert([
            'name' => 'Steven Smith',
            'address' => 'mandaluyong City',
            'created_at' => Carbon::now() ->format('y-m-d H:i:s'),
            'updated_at' => Carbon::now() ->format('y-m-d H:i:s'),
        ]);
    }
}
