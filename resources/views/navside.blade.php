<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">

            <li class="sidebar-item"> <a class="sidebar-link waves-effect
             waves-dark sidebar-link" href="/about" 
            aria-expanded="false"><i class="mdi mdi-account-card-details">
            </i><span class="hide-menu">About Us</span></a></li>

            <li class="sidebar-item"> <a class="sidebar-link waves-effect
             waves-dark sidebar-link" href="/contact" 
            aria-expanded="false"><i class="mdi mdi-contacts">
            </i><span class="hide-menu">Contacts</span></a></li>

            <li class="sidebar-item"> <a class="sidebar-link waves-effect
             waves-dark sidebar-link" href="/customers" 
            aria-expanded="false"><i class="mdi mdi-account-multiple">
            </i><span class="hide-menu">Customers</span></a></li>

            <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('logout') }}" 
            aria-expanded="false" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="mdi mdi-arrow-left"> </i>  {{ __('Logout') }}
                </li>
                </a>
            <form id ="logout-form" action="{{ route('logout') }} " method="POST" sytle="display:none;">
            @csrf
            
            </form>
     
            </ul>


        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
