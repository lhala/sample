@extends('layout')
@section('content')
<h1>{{$title}}</h1>
<p>
Developer Economics is the largest developer research program, engaging thousands of developers of all shapes and sizes across the globe. Our vision is to help developers make better business decisions. Our aim is to improve developers' understanding of current trends in their industry, learn more about topics they're interested in and have more information and data to use in their decision making. As part of the research program, we make sure we return value to all developers, whether part of our own or the global community.

Our global surveys reach over 40,000 developers each year across 160+ countries. We explore trends across different types of development, from hobby to professional and further, in mobile, IoT, desktop, cloud, web, AR/VR, Machine Learning, Data Science and game development. Among other aspects, Developer Economics tracks developer experience across platforms, languages, revenue models, verticals, tools, segments and regions.

On this website you will find the following:</p>


<p>free research reports for developers, with analysis of key trends & based on data from our global developer surveys
key trends in development, depicted in graphs which are free to view and relevant to all sectors of development examined in our survey
a blog with insight into developer tools, developers' lives and trending techniques and practices
lists of tools featured in our surveys
</p>
@endsection