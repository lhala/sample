<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    
        public function display()
        {
            $title ="Contact Us";
            return view('contact',
            compact('title'));
            
    }
    
}
